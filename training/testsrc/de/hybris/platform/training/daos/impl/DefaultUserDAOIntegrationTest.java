/**
 *
 */
package de.hybris.platform.training.daos.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalBaseTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.training.daos.UserDAO;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;


/**
 *
 */
@IntegrationTest
public class DefaultUserDAOIntegrationTest extends ServicelayerTransactionalBaseTest
{
	private final static String USER_UID = "01";
	private final static String USER_NAME = "alex";
	@Resource
	private UserDAO userDAO;

	@Resource
	ModelService modelService;

	@Test
	public void testUserDAO()
	{
		List<UserModel> usersByUid = userDAO.findUserByUid(USER_UID);
		assertTrue("No User should be returned", usersByUid.isEmpty());

		List<UserModel> allUser = userDAO.findUsers();
		final int oldSize = allUser.size();

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_UID);
		userModel.setName(USER_NAME);
		modelService.save(userModel);
		allUser = userDAO.findUsers();
		assertTrue("Number of users increased by 1", allUser.size() == oldSize + 1);

		usersByUid = userDAO.findUserByUid(USER_UID);
		assertTrue("The number of users found by name is 1", usersByUid.size() == 1);
		assertEquals("The name of the user found is correct", USER_NAME, usersByUid.get(0).getName());
		assertEquals("the uid of the user found is correct", USER_UID, usersByUid.get(0).getUid());

	}

	@Test
	public void testFindUsers_EmptyStringParam()
	{
		final List<UserModel> users = userDAO.findUserByUid("");
		assertTrue("No user should be returned", users.isEmpty());
	}


	@Test(expected = IllegalArgumentException.class)
	public void testfindUsers_NullParam()
	{
		userDAO.findUserByUid(null);
	}


}
