/**
 *
 */
package de.hybris.platform.training.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.training.daos.UserDAO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;




/**
 *
 */
public class DefaultUserServiceUnitTest
{
	private DefaultTrainingUserService userService;
	private UserDAO userDAO;
	private UserModel userModel;
	private final static String USER_UID = "01";
	private final static String USER_NAME = "alex";

	@Before
	public void setUp()
	{
		userService = new DefaultTrainingUserService();
		userDAO = mock(UserDAO.class);
		userService.setUserDAO(userDAO);
		userModel = new UserModel();
		userModel.setUid(USER_UID);
		userModel.setName(USER_NAME);
	}

	@Test
	public void testGetUsers()
	{
		final List<UserModel> userModels = Arrays.asList(userModel);
		when(userDAO.findUsers()).thenReturn(userModels);
		final List<UserModel> result = userService.getUsers();
		assertEquals("We should find one", 1, result.size());
		assertEquals("And should equals what the mock returned", userModel, result.get(0));
	}

	@Test
	public void testGetUserForUid()
	{
		when(userDAO.findUserByUid(USER_UID)).thenReturn(Collections.singletonList(userModel));
		final UserModel result = userService.getUserForUid(USER_UID);
		assertEquals("User should equals() what the mock returned", userModel, result);
	}
}
