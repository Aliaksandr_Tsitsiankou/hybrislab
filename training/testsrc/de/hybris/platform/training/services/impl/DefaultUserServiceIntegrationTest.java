/**
 *
 */
package de.hybris.platform.training.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.training.services.TrainingUserService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;


/**
 *
 */
@IntegrationTest
public class DefaultUserServiceIntegrationTest extends ServicelayerTransactionalTest
{


	@Resource
	public TrainingUserService dUserService;

	@Resource
	private ModelService modelService;

	private UserModel userModel;
	private final static String USER_UID = "01";
	private final static String USER_NAME = "alex";

	@Before
	public void setUp()
	{
		userModel = new UserModel();
		userModel.setUid(USER_UID);
		userModel.setName(USER_NAME);
	}


	@Test(expected = UnknownIdentifierException.class)
	public void testFileUid()
	{
		dUserService.getUserForUid(USER_UID);
	}

	@Test
	public void testUserService()
	{
		List<UserModel> userModels = dUserService.getUsers();
		final int oldSize = userModels.size();
		modelService.save(userModel);
		userModels = dUserService.getUsers();
		assertTrue("Number of users increased by 1", userModels.size() == oldSize + 1);
		final UserModel persistedUserModel = dUserService.getUserForUid(USER_UID);
		assertEquals("Different user found", userModel, persistedUserModel);
	}

}
