/**
 *
 */
package de.hybris.platform.training.facades.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.training.data.UserData;
import de.hybris.platform.training.facades.UserFacade;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;


/**
 *
 */
@IntegrationTest
public class DefaultUserFacadeIntegrationTest extends ServicelayerTransactionalTest
{

	@Resource
	private UserFacade userFacade;

	@Resource
	private ModelService modelService;

	private UserModel userModel;
	private UserData originalUserData;
	private final static String USER_UID = "01";
	private final static String USER_NAME = "alex";
	private final static String USER_PASSWORD_QUESTION = "What is the name of the capital of Great Britain?";
	private final static String USER_DESCRIPTION = "I was born in London:)";

	@Before
	public void setUp()
	{
		userModel = new UserModel();
		userModel.setUid(USER_UID);
		userModel.setName(USER_NAME);
		userModel.setPasswordQuestion(USER_PASSWORD_QUESTION);
		userModel.setDescription(USER_DESCRIPTION);
		originalUserData = new UserData();
		originalUserData.setUid(USER_UID);
		originalUserData.setName(USER_NAME);
		originalUserData.setPasswordQuestion(USER_PASSWORD_QUESTION);
		originalUserData.setDescription(USER_DESCRIPTION);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncorrectParameter()
	{
		userFacade.getUser(USER_UID);
	}

	@Test(expected = NullPointerException.class)
	public void testNullParameter()
	{
		userFacade.getUser(null);
	}

	@Test
	public void testUserFacade()
	{
		List<UserData> userDatas = userFacade.getUsers();
		assertNotNull(userDatas);
		final int oldSize = userDatas.size();
		modelService.save(userModel);
		userDatas = userFacade.getUsers();
		assertTrue("Number of users increased by 1", userDatas.size() == oldSize + 1);
		assertEquals(USER_NAME, userDatas.get(oldSize).getName());
		assertEquals(USER_UID, userDatas.get(oldSize).getUid());
		assertEquals(USER_PASSWORD_QUESTION, userDatas.get(oldSize).getPasswordQuestion());
		assertEquals(USER_DESCRIPTION, userDatas.get(oldSize).getDescription());
		final UserData userData = userFacade.getUser(USER_UID);
		assertEquals("Not the same object that was added", originalUserData, userData);
	}

}
