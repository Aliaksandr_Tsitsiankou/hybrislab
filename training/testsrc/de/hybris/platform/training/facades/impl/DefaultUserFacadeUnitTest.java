/**
 *
 */
package de.hybris.platform.training.facades.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.training.converters.UserConverter;
import de.hybris.platform.training.data.UserData;
import de.hybris.platform.training.services.TrainingUserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


/**
 *
 */
public class DefaultUserFacadeUnitTest
{
	private DefaultUserFacade userFacade;
	private TrainingUserService userService;
	private UserData originalUserData;
	private final static String USER_UID = "01";
	private final static String USER_NAME = "alex";
	private final static String USER_PASSWORD_QUESTION = "What is the name of the capital of Great Britain?";
	private final static String USER_DESCRIPTION = "I was born in London:)";

	private List<UserModel> dummyDataUsers()
	{
		final UserModel user = new UserModel();
		user.setUid(USER_UID);
		user.setName(USER_NAME);
		user.setPasswordQuestion(USER_PASSWORD_QUESTION);
		user.setDescription(USER_DESCRIPTION);
		final List<UserModel> users = new ArrayList<UserModel>();
		users.add(user);
		return users;
	}

	private UserModel dummyDataUser()
	{
		final UserModel user = new UserModel();
		user.setUid(USER_UID);
		user.setName(USER_NAME);
		user.setPasswordQuestion(USER_PASSWORD_QUESTION);
		user.setDescription(USER_DESCRIPTION);
		return user;
	}

	@Before
	public void seUp()
	{
		userFacade = new DefaultUserFacade();
		userService = mock(TrainingUserService.class);
		userFacade.setUserService(userService);
		userFacade.setUserConverter(new UserConverter());
		originalUserData = new UserData();
		originalUserData.setUid(USER_UID);
		originalUserData.setName(USER_NAME);
		originalUserData.setPasswordQuestion(USER_PASSWORD_QUESTION);
		originalUserData.setDescription(USER_DESCRIPTION);
	}

	@Test
	public void testGetUsers()
	{
		final List<UserModel> users = dummyDataUsers();
		when(userService.getUsers()).thenReturn(users);
		final List<UserData> dto = userFacade.getUsers();
		assertNotNull(dto);
		assertEquals(users.size(), dto.size());
		assertEquals(originalUserData, dto.get(0));
	}

	@Test
	public void testGetUser()
	{
		final UserModel user = dummyDataUser();
		when(userService.getUserForUid(USER_UID)).thenReturn(user);
		final UserData dto = userFacade.getUser(USER_UID);
		assertEquals(originalUserData, dto);

	}
}
