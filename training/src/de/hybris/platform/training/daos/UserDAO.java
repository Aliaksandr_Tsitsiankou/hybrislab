/**
 *
 */
package de.hybris.platform.training.daos;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;

import java.util.List;


/**
 *
 */
public interface UserDAO extends UserDao
{
	public List<UserModel> findUsers();

	public List<UserModel> findUserByUid(String uid);
}
