/**
 *
 */
package de.hybris.platform.training.daos.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.training.daos.ProductDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;


/**
 *
 */
@Component(value = "productDAO")
public class DefaultProductDAO implements ProductDAO
{
	private final static String SELECT_COUNT_PRODUCT = "SELECT COUNT(*) FROM {" + ProductModel._TYPECODE + "}";
	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@SuppressWarnings("unchecked")
	@Override
	public int amountProducts()
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(SELECT_COUNT_PRODUCT);
		flexibleSearchQuery.setResultClassList(Lists.newArrayList(Integer.class));
		return (Integer) flexibleSearchService.search(flexibleSearchQuery).getResult().get(0);
	}

}
