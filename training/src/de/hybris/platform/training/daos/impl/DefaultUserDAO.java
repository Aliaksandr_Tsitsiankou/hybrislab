/**
 *
 */
package de.hybris.platform.training.daos.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;
import de.hybris.platform.training.daos.UserDAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 */
public class DefaultUserDAO extends DefaultUserDao implements UserDAO
{
	private final static String SELECT_ALL_USERS = "SELECT {p:" + UserModel.PK + "} " + "FROM {" + UserModel._TYPECODE + " AS p} ";
	private final static String SELECT_USER_FOR_UID = "SELECT {p:" + UserModel.PK + "} " + "FROM {" + UserModel._TYPECODE
			+ " AS p} WHERE {p:" + UserModel.UID + "}=?uid";

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<UserModel> findUsers()
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(SELECT_ALL_USERS);
		return flexibleSearchService.<UserModel> search(flexibleSearchQuery).getResult();
	}

	@Override
	public List<UserModel> findUserByUid(final String uid)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(SELECT_USER_FOR_UID);
		flexibleSearchQuery.addQueryParameter("uid", uid);
		return flexibleSearchService.<UserModel> search(flexibleSearchQuery).getResult();
	}
}
