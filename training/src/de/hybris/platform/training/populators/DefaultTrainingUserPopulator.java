/**
 *
 */
package de.hybris.platform.training.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.training.data.UserData;

import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultTrainingUserPopulator implements Populator<UserModel, UserData>
{
	public static final String ERROR_MESSAGE_GET = "Item model is required";

	@Override
	public void populate(final UserModel source, final UserData target) throws ConversionException
	{
		Preconditions.checkNotNull(source, ERROR_MESSAGE_GET);
		Preconditions.checkNotNull(target, ERROR_MESSAGE_GET);
		target.setUid(source.getUid());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setPasswordQuestion(source.getPasswordQuestion());
	}
}
