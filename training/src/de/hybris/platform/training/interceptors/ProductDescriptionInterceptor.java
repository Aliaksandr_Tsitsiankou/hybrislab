/**
 *
 */
package de.hybris.platform.training.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class ProductDescriptionInterceptor implements ValidateInterceptor<ProductModel>
{
	private final static String CHECKED_CHARACTER = "_";
	private final static String PATERN_INFO_MESSAGE = "The product with the code: {} and the name: {} contains the character {} in the description. Description: {}.";
	final Logger logger = LoggerFactory.getLogger(ProductDescriptionInterceptor.class);

	@Override
	public void onValidate(final ProductModel arg0, final InterceptorContext arg1) throws InterceptorException
	{
		final String description = arg0.getDescription();
		if (description != null && description.contains(CHECKED_CHARACTER))
		{
			logger.info(PATERN_INFO_MESSAGE, arg0.getCode(), arg0.getName(), CHECKED_CHARACTER, arg0.getDescription());
		}
	}
}
