/**
 *
 */
package de.hybris.platform.training.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.training.daos.ProductDAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 */
public class PrinAmountProductsJob extends AbstractJobPerformable<CronJobModel>
{
	private final static String MESSAGE = "The amount of Products : {}";
	final Logger logger = LoggerFactory.getLogger(PrinAmountProductsJob.class);

	@Autowired
	private ProductDAO productDAO;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		logger.info(MESSAGE, productDAO.amountProducts());
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
