/**
 *
 */
package de.hybris.platform.training.listeners;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 */
public class ProductUpdateDataListener implements AfterSaveListener
{
	private final static int PRODUCT_TYPE_CODE = 1;
	private final static String PRODUCT_WORKFLOW_TEMPLATE_CODE = "ProductUpdateData";
	private final static String PATTERN_MESSAGE_RESULT = "Product update data. Product name {} , product code: {}";

	final Logger logger = LoggerFactory.getLogger(ProductUpdateDataListener.class);
	@Autowired
	private WorkflowService workflowService;
	@Autowired
	private WorkflowTemplateService workflowTemplateService;
	@Autowired
	private WorkflowProcessingService workflowProcessingService;
	@Autowired
	private UserService userService;
	@Autowired
	private ModelService modelService;

	@Override
	public void afterSave(final Collection<AfterSaveEvent> arg0)
	{
		for (final AfterSaveEvent afterSaveEvent : arg0)
		{
			final PK pk = afterSaveEvent.getPk();
			if (pk.getTypeCode() == PRODUCT_TYPE_CODE)
			{
				final ProductModel productModel = modelService.get(pk);
				final WorkflowTemplateModel workflowTemplateModel = this.workflowTemplateService
						.getWorkflowTemplateForCode(PRODUCT_WORKFLOW_TEMPLATE_CODE);
				final WorkflowModel workflowModel = this.workflowService.createWorkflow(workflowTemplateModel, productModel,
						userService.getAdminUser());
				modelService.save(workflowModel);
				for (final WorkflowActionModel workflowActionModel : workflowModel.getActions())
				{
					modelService.save(workflowActionModel);
				}
				this.workflowProcessingService.startWorkflow(workflowModel);
				logger.info(PATTERN_MESSAGE_RESULT, productModel.getName(), productModel.getCode());
			}
		}
	}
}
