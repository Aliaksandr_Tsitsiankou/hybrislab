/**
 *
 */
package de.hybris.platform.training.setup;

import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.training.constants.TrainingConstants;
import de.hybris.platform.util.CSVReader;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
@SystemSetup(extension = TrainingConstants.EXTENSIONNAME)
public class TrainingSystemSetup
{
	private final static String FILE_PATH = "/impex/custom/";
	private final static String FILE_NAME_UPDATE = "custom-update-description.impex";
	private final static String FILE_NAME_INIT = "custom-init-description.impex";
	private final static String ENCODING = "utf-8";
	private final static String ERROR_MESSAGE_FILE_NOT_FOUND = "File not found";
	private final static String ERROR_MESSAGE_ENCODING = "The Character Encoding is not supported";
	private final static String ERROR_MESSAGE_IMPEX = "Some problems when calling importAll().";
	final Logger logger = LoggerFactory.getLogger(TrainingSystemSetup.class);

	@SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = Process.UPDATE)
	public void updateDescriptionDuringUpdate()
	{
		importImpexFile(FILE_NAME_UPDATE);
	}

	@SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = Process.INIT)
	public void updateDescriptionDuringInit()
	{
		importImpexFile(FILE_NAME_INIT);
	}

	private void importImpexFile(final String fileName)
	{
		final String impex = ImpExManager.class.getResource(FILE_PATH + fileName).getPath();
		CSVReader csvReader = null;
		try
		{
			csvReader = new CSVReader(impex, ENCODING);
			final Importer importer = new Importer(csvReader);
			importer.importAll();
		}
		catch (final UnsupportedEncodingException e)
		{
			logger.error(ERROR_MESSAGE_ENCODING, e);
		}
		catch (final FileNotFoundException e)
		{
			logger.error(ERROR_MESSAGE_FILE_NOT_FOUND, e);
		}
		catch (final ImpExException e)
		{
			logger.error(ERROR_MESSAGE_IMPEX, e);
		}
	}
}
