/**
 *
 */
package de.hybris.platform.training.servicelayer.model.attribute;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import com.google.common.base.Preconditions;


public class CustomerDescriptionAttributeHandler implements DynamicAttributeHandler<String, CustomerModel>
{

	public static final String PATTERN_RESULT = "%s:%s.Order quantity is <%d>";
	public static final String ERROR_MESSAGE_GET = "Item model is required";
	public static final String ERROR_MESSAGE_SET = "Write operation is not supported";

	@Override
	public String get(final CustomerModel model)
	{
		Preconditions.checkNotNull(model, ERROR_MESSAGE_GET);
		return String.format(PATTERN_RESULT, model.getName(), model.getEmail(), model.getOrders().size());
	}

	@Override
	public void set(final CustomerModel model, final String s)
	{
		throw new UnsupportedOperationException(ERROR_MESSAGE_SET);
	}
}
