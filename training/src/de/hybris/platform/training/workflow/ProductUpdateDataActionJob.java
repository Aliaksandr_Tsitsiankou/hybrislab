/**
 *
 */
package de.hybris.platform.training.workflow;

import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 *
 */
@Component
public class ProductUpdateDataActionJob implements AutomatedWorkflowTemplateJob
{
	private final static String MESSAGE = "Workflow : ProductUpdateDataActionJob";
	final Logger logger = LoggerFactory.getLogger(ProductUpdateDataActionJob.class);

	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel arg0)
	{
		logger.info(MESSAGE);
		for (final WorkflowDecisionModel decision : arg0.getDecisions())
		{
			return decision;
		}
		return null;
	}

}
