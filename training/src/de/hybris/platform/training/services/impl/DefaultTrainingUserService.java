/**
 *
 */
package de.hybris.platform.training.services.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import de.hybris.platform.training.daos.UserDAO;
import de.hybris.platform.training.services.TrainingUserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 *
 */

public class DefaultTrainingUserService extends DefaultUserService implements TrainingUserService
{
	private final static String ERROR_MESAGE_USER_NOT_FOUND = "User with uid :%s not found!";
	private final static String ERROR_MESAGE_USER_UID_NOT_UNIQUE = "User uid :%s is not unique, %d user found!";
	private UserDAO userDAO;

	@Override
	public List<UserModel> getUsers()
	{
		return userDAO.findUsers();
	}


	@Override
	public UserModel getUserForUid(final String userUid)
	{
		final List<UserModel> result = userDAO.findUserByUid(userUid);
		if (result.isEmpty())
		{
			throw new UnknownIdentifierException(String.format(ERROR_MESAGE_USER_NOT_FOUND, userUid));
		}
		else if (result.size() > 1)
		{
			throw new AmbiguousIdentifierException(String.format(ERROR_MESAGE_USER_UID_NOT_UNIQUE, userUid, result.size()));
		}
		return result.get(0);
	}

	@Required
	public void setUserDAO(final UserDAO userDAO)
	{
		this.userDAO = userDAO;
	}

}
