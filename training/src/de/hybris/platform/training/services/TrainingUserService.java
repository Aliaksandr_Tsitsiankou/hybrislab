/**
 *
 */
package de.hybris.platform.training.services;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;


/**
 *
 */
public interface TrainingUserService extends UserService
{
	public List<UserModel> getUsers();

	public UserModel getUserForUid(String userUid);
}
