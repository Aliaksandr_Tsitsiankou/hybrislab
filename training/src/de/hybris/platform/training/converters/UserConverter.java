/**
 *
 */
package de.hybris.platform.training.converters;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.training.data.UserData;


/**
 *
 */
public class UserConverter extends AbstractPopulatingConverter<UserModel, UserData>
{
}
