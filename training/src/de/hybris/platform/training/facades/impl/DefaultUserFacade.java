/**
 *
 */
package de.hybris.platform.training.facades.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.training.data.UserData;
import de.hybris.platform.training.facades.UserFacade;
import de.hybris.platform.training.services.TrainingUserService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultUserFacade implements UserFacade
{
	public static final String ERROR_MESSAGE_UID_NULL = "The argument uid must not be equal to NULL";
	public static final String ERROR_MESSAGE_UID_INCORRECT = "Argument:%s is incorrect";

	private TrainingUserService userService;
	private Converter<UserModel, UserData> userConverter;

	@Override
	public List<UserData> getUsers()
	{
		final List<UserModel> userModels = userService.getUsers();
		final List<UserData> userDatas = new ArrayList<UserData>();
		for (final UserModel model : userModels)
		{
			userDatas.add(userConverter.convert(model));
		}

		return userDatas;
	}


	@Override
	public UserData getUser(final String uid)
	{
		Preconditions.checkNotNull(uid, ERROR_MESSAGE_UID_NULL);
		try
		{
			final UserModel userModel = userService.getUserForUid(uid);
			return userConverter.convert(userModel);
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw new IllegalArgumentException(String.format(ERROR_MESSAGE_UID_INCORRECT, uid), e);
		}
	}

	@Required
	public void setUserService(final TrainingUserService userService)
	{
		this.userService = userService;
	}

	@Required
	public void setUserConverter(final Converter<UserModel, UserData> userConverter)
	{
		this.userConverter = userConverter;
	}
}
