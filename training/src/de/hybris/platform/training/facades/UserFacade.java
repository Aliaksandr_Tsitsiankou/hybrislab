/**
 *
 */
package de.hybris.platform.training.facades;

import de.hybris.platform.training.data.UserData;

import java.util.List;


/**
 *
 */
public interface UserFacade
{
	public List<UserData> getUsers();

	public UserData getUser(String uid);
}
