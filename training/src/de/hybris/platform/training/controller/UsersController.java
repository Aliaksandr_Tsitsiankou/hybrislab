/**
 *
 */
package de.hybris.platform.training.controller;

import de.hybris.platform.training.data.UserData;
import de.hybris.platform.training.facades.UserFacade;
import de.hybris.platform.training.helper.UserUidEncoded;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 *
 */
@Controller
public class UsersController
{
	private UserFacade userFacade;

	@RequestMapping(value = "/users")
	public String showUsers(final Model model)
	{
		final List<UserData> users = userFacade.getUsers();
		model.addAttribute("users", users);
		return "UserListing";
	}

	@RequestMapping(value = "/users/{userUid}")
	public String showStadiumDetails(@PathVariable String userUid, final Model model) throws UnsupportedEncodingException
	{
		userUid = URLDecoder.decode(userUid, "UTF-8");
		final UserData user = userFacade.getUser(userUid);
		user.setUid(UserUidEncoded.getUidEncoded(user.getUid()));
		model.addAttribute("user", user);
		return "UserDetails";
	}

	@Autowired
	public void setUserFacade(final UserFacade userFacade)
	{
		this.userFacade = userFacade;
	}

}
