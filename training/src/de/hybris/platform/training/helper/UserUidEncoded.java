/**
 *
 */
package de.hybris.platform.training.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class UserUidEncoded
{
	private final static Logger LOG = LoggerFactory.getLogger(UserUidEncoded.class);
	private final static String ERROR_MESSAGE = "Problems while encoding: {}";

	public static String getUidEncoded(final String uid)
	{
		try
		{
			return URLEncoder.encode(uid, "UTF-8");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(ERROR_MESSAGE, e);
			return "";
		}
	}
}
