/**
 *
 */
package de.hybris.platform.training.decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;


/**
 *
 */
public class ProductDescriptionDecorator implements CSVCellDecorator
{
	private final static String ADDED_CHARACTER = "_";

	@Override
	public String decorate(final int arg0, final Map<Integer, String> arg1)
	{
		return arg1.get(arg0) + ADDED_CHARACTER;
	}

}
