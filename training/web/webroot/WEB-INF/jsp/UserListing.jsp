<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <title>User Listing</title>
    <body>
        <h1>User Listing</h1>
     <ul>
     <c:forEach var="user" items="${users}">
        <li><a href="./users/${user.uid}">${user.name}</a></li>
      </c:forEach>
      </ul>
    </body>
</html>