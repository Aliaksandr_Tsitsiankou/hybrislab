package org.customeraddon.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;


public class CustomCustomerDataPopulator extends CustomerPopulator {

    @Override
    public void populate(final CustomerModel source, final CustomerData target) {
        super.populate(source, target);
        target.setHobby(source.getHobby());
    }
}
