package org.customeraddon.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class CustomCustomerDataReversePopulator extends CustomerReversePopulator {

    @Override
    public void populate(final CustomerData source, final CustomerModel target) throws ConversionException {
        super.populate(source, target);
        target.setHobby(source.getHobby());
    }
}
