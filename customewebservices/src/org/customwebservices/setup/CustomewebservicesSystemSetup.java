/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.customwebservices.setup;

import static org.customwebservices.constants.CustomewebservicesConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import org.customwebservices.constants.CustomewebservicesConstants;
import org.customwebservices.service.CustomewebservicesService;


@SystemSetup(extension = CustomewebservicesConstants.EXTENSIONNAME)
public class CustomewebservicesSystemSetup
{
	private final CustomewebservicesService customewebservicesService;

	public CustomewebservicesSystemSetup(final CustomewebservicesService customewebservicesService)
	{
		this.customewebservicesService = customewebservicesService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		customewebservicesService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return CustomewebservicesSystemSetup.class.getResourceAsStream("/customewebservices/sap-hybris-platform.png");
	}
}
