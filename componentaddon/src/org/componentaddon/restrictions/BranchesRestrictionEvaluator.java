/**
 *
 */
package org.componentaddon.restrictions;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.commercefacades.customer.CustomerFacade;

import org.componentaddon.model.BranchesRestrictionModel;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Alex
 *
 */
public class BranchesRestrictionEvaluator implements CMSRestrictionEvaluator<BranchesRestrictionModel> {
    private CustomerFacade customerFacade;

    @Override
    public boolean evaluate(BranchesRestrictionModel branchesRestrictionModel, RestrictionData restrictionData) {
        if (customerFacade.getCurrentCustomer() != null) {
            return customerFacade.getCurrentCustomer().getCurrency().getName().equals("Pound");
        }
        return true;
    }

    @Required
    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }
}
