/**
 *
 */
package org.mywebservice.controllers;

import static org.mywebservice.constants.MywebserviceConstants.CLIENT_CREDENTIAL_AUTHORIZATION_NAME;
import static org.mywebservice.constants.MywebserviceConstants.PASSWORD_AUTHORIZATION_NAME;

import de.hybris.platform.webservicescommons.mapping.DataMapper;

import java.util.List;

import javax.annotation.Resource;

import org.mywebservice.data.MyTargetData;
import org.mywebservice.data.MyTargetDataList;
import org.mywebservice.dto.MyTargetDTO;
import org.mywebservice.facades.impl.DefaultMyTargetFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;


/**
 * @author Alex
 *
 */
@Controller
@RequestMapping(value = "/")
public class DefaultController
{
	@Resource(name = "myTargetFacade")
	private DefaultMyTargetFacade myTargetFacade;

	@Resource(name = "dataMapper")
	private DataMapper dataMapper;


	@RequestMapping(value = "/myTargets", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "Get myTargets", notes = "method returning myTargets list.", produces = "application/json,application/xml", authorizations =
	{ @Authorization(value = CLIENT_CREDENTIAL_AUTHORIZATION_NAME), @Authorization(value = PASSWORD_AUTHORIZATION_NAME) })
	public MyTargetDTO getMyTargets()
	{
		final List<MyTargetData> myTargetData = myTargetFacade.getMyTargets();
		final MyTargetDataList myTargetDataList = new MyTargetDataList();
		myTargetDataList.setMyTargets(myTargetData);
		return dataMapper.map(myTargetDataList, MyTargetDTO.class);
	}

	@RequestMapping(value = "/myTargets/{name}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "Get myTarget by name", notes = "method returning myTarget.", produces = "application/json,application/xml", authorizations =
	{ @Authorization(value = CLIENT_CREDENTIAL_AUTHORIZATION_NAME), @Authorization(value = PASSWORD_AUTHORIZATION_NAME) })
	public MyTargetDTO getAchievement(@ApiParam(value = "User identifier", required = true) @PathVariable final String name)
	{
		return dataMapper.map(myTargetFacade.getMyTarget(name), MyTargetDTO.class);
	}
}
