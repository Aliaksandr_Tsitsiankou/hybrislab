/**
 *
 */
package org.mywebservice.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.mywebservice.data.MyTargetData;
import org.mywebservice.model.MyTargetModel;
import org.springframework.util.Assert;


/**
 * @author Alex
 *
 */
public class MyTargetPopulator implements Populator<MyTargetModel, MyTargetData>
{

	@Override
	public void populate(final MyTargetModel source, final MyTargetData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setName(source.getName());
		target.setDescription(source.getDescription());
	}

}
