/**
 *
 */
package org.mywebservice.facades;

import java.util.List;

import org.mywebservice.data.MyTargetData;


/**
 * @author Alex
 *
 */
public interface MyTargetFacade
{
	MyTargetData getMyTarget(String name);

	List<MyTargetData> getMyTargets();
}
