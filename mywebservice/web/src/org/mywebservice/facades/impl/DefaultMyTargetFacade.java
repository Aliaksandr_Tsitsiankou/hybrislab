/**
 *
 */
package org.mywebservice.facades.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.mywebservice.data.MyTargetData;
import org.mywebservice.facades.MyTargetFacade;
import org.mywebservice.model.MyTargetModel;
import org.mywebservice.services.MyTargetService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.access.annotation.Secured;

import com.google.common.base.Preconditions;


/**
 * @author Alex
 *
 */
public class DefaultMyTargetFacade implements MyTargetFacade
{
	public static final String ERROR_MESSAGE_MANE_NULL = "The argument name must not be equal to NULL";
	private MyTargetService myTargetService;
	private Converter<MyTargetModel, MyTargetData> myTargetConverter;


	@Override
	@Secured("ROLE_CLIENT")
	public MyTargetData getMyTarget(final String name)
	{
		Preconditions.checkNotNull(name, ERROR_MESSAGE_MANE_NULL);
		final MyTargetModel myTargetModel = getMyTargetService().getMyTarget(name);
		return getMyTargetConverter().convert(myTargetModel);
	}


	@Override
	@Secured("ROLE_CLIENT")
	public List<MyTargetData> getMyTargets()
	{
		final Collection<MyTargetModel> myTargetModels = getMyTargetService().getMyTargets();
		return myTargetModels.stream().map(a -> getMyTargetConverter().convert(a)).collect(Collectors.toList());

	}


	@Required
	public void setMyTargetService(final MyTargetService myTargetService)
	{
		this.myTargetService = myTargetService;
	}

	@Required
	public void setMyTargetConverter(final Converter<MyTargetModel, MyTargetData> myTargetConverter)
	{
		this.myTargetConverter = myTargetConverter;
	}

	public MyTargetService getMyTargetService()
	{
		return myTargetService;
	}


	public Converter<MyTargetModel, MyTargetData> getMyTargetConverter()
	{
		return myTargetConverter;
	}

}
