/**
 *
 */
package org.mywebservice.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import org.assertj.core.util.Lists;
import org.mywebservice.dao.MyTargetDAO;
import org.mywebservice.model.MyTargetModel;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Alex
 *
 */
public class DefaultMyTargetDAO implements MyTargetDAO
{

	@Autowired
	private FlexibleSearchService flexibleSearchService;
	private final String SELECT_ALL_MY_TARGETS = String.format("SELECT {p: %s} FROM {%s As p}", MyTargetModel.PK,
			MyTargetModel._TYPECODE);
	private final String SELECT_MY_TARGET_FOR_NAME = String.format("SELECT {p: %s} FROM {%s As p} WHERE {p: %s} LIKE ?name",
			MyTargetModel.PK, MyTargetModel._TYPECODE, MyTargetModel.NAME);

	@Override
	public MyTargetModel getMyTarget(final String name)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SELECT_MY_TARGET_FOR_NAME);
		query.addQueryParameter("name", name);
		query.setResultClassList(Lists.newArrayList(MyTargetModel.class));
		final MyTargetModel myTargetModel = (MyTargetModel) flexibleSearchService.search(query).getResult().get(0);
		return myTargetModel;
	}


	@Override
	public List<MyTargetModel> getMyTargets()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SELECT_ALL_MY_TARGETS);
		return flexibleSearchService.<MyTargetModel> search(query).getResult();
	}

}
