/**
 *
 */
package org.mywebservice.dao;

import java.util.List;

import org.mywebservice.model.MyTargetModel;


/**
 * @author Alex
 *
 */
public interface MyTargetDAO
{
	MyTargetModel getMyTarget(String name);

	List<MyTargetModel> getMyTargets();
}
