/**
 *
 */
package org.mywebservice.services;

import java.util.List;

import org.mywebservice.model.MyTargetModel;


/**
 * @author Alex
 *
 */
public interface MyTargetService
{
	MyTargetModel getMyTarget(String name);

	List<MyTargetModel> getMyTargets();
}
