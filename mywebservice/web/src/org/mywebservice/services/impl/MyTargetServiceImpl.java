/**
 *
 */
package org.mywebservice.services.impl;

import java.util.List;

import org.mywebservice.dao.MyTargetDAO;
import org.mywebservice.model.MyTargetModel;
import org.mywebservice.services.MyTargetService;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Alex
 *
 */
public class MyTargetServiceImpl implements MyTargetService
{
	private MyTargetDAO myTargetDAO;

	@Override
	public MyTargetModel getMyTarget(final String name)
	{

		return myTargetDAO.getMyTarget(name);
	}

	@Override
	public List<MyTargetModel> getMyTargets()
	{

		return myTargetDAO.getMyTargets();
	}

	@Required
	public void setMyTargetDAO(final MyTargetDAO myTargetDAO)
	{
		this.myTargetDAO = myTargetDAO;
	}

}
